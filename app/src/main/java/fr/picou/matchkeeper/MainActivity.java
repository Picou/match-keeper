package fr.picou.matchkeeper;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.picou.matchkeeper.database.SQLiteHelper;
import fr.picou.matchkeeper.database.model.Match;
import fr.picou.matchkeeper.database.model.Team;
import fr.picou.matchkeeper.new_match.AddNewMatchActivity;
import fr.picou.matchkeeper.settings.SettingsActivity;
import fr.picou.matchkeeper.stats.OnStatFragmentInteractionListener;
import fr.picou.matchkeeper.stats.StatFragment;

public class MainActivity extends AppCompatActivity implements OnHomeFragmentInteractionListener, OnMatchFragmentInteractionListener, OnStatFragmentInteractionListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //bottom nav
        this.configureBottomView();

        final HomeFragment frag = new HomeFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.nav_host_fragment, frag).commit();
    }

    private void configureBottomView(){
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return MainActivity.this.switchScreen(item.getItemId());
            }
        });
    }

    private boolean switchScreen(Integer idMenu){
        Fragment frag;
        if(idMenu == R.id.settingsMenu){
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
        }
        switch (idMenu) {
            case R.id.homeMenu:
            default:
                frag = new HomeFragment();
                break;
            case R.id.matchMenu:
                frag = new MatchFragment();
                break;
            case R.id.statMenu:
                frag = new StatFragment();
                break;
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, frag);
        transaction.commit();
        return true;
    }

    @Override
    public void displayStatFragment() {
        switchScreen(R.id.statMenu);
    }

    @Override
    public void extractingStats() {
        // for this version, we assume that we only want to check the stats of the first team
        SQLiteHelper sqLiteHelper = new SQLiteHelper(this);
        ArrayList<Match> listMatches = sqLiteHelper.getMatches();
        CalculAsyncTask asyncStatsTask = new CalculAsyncTask();
        asyncStatsTask.execute(listMatches.toArray(new Match[listMatches.size()]));
    }



    @Override
    public void displayAddNewMatchActivity() {
        Intent addNewMatchIntent = new Intent(this, AddNewMatchActivity.class);
        startActivity(addNewMatchIntent);
    }

    @Override
    public void displayLastMatch() {
        SQLiteHelper sqLiteHelper = new SQLiteHelper(this);
        Match match = sqLiteHelper.getLatestMatch();
        if(match != null) {
            Team team1 = sqLiteHelper.getTeam(match.getTeam1());
            Team team2 = sqLiteHelper.getTeam(match.getTeam2());
            TextView txtScore = findViewById(R.id.textLastScore);
            txtScore.setText(match.getScore1() + " - " + match.getScore2());
            TextView txtTeam1 = findViewById(R.id.textTeam1LastScore);
            txtTeam1.setText(team1.getName());
            TextView txtTeam2 = findViewById(R.id.textTeam2LastScore);
            txtTeam2.setText(team2.getName());
        }
        sqLiteHelper.close();
    }



    public Fragment getVisibleFragment(){
        FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if(fragments != null){
            for(Fragment fragment : fragments){
                if(fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if(getVisibleFragment().getClass() == HomeFragment.class) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.nav_host_fragment, new HomeLandscapeFragment());
                transaction.commit();
            }

        }
        else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            if(getVisibleFragment().getClass() == HomeLandscapeFragment.class) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.nav_host_fragment, new HomeFragment());
                transaction.commit();
            }
        }
    }

    private class CalculAsyncTask extends AsyncTask<Match, Integer, Void> {
        @Override
        protected Void doInBackground(Match... matchs) {
            ArrayList<Match> listMatch = new ArrayList<>(Arrays.asList(matchs));
            int tries = calculNbTry(listMatch);
            int penalties = calculNbPenalty(listMatch);
            int drops = calculNbDrop(listMatch);
            int avg = calculAvgScore(listMatch);
            int avgOpp = calculAvgOpponent(listMatch);
            publishProgress(tries, penalties, drops, avg, avgOpp);
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... result) {
            //used instead of onPostExecute because there are several results to return
            TextView tvTries = findViewById(R.id.NumberTry);
            TextView tvPenalties = findViewById(R.id.NumberPenalty);
            TextView tvDrops = findViewById(R.id.NumberDrop);
            TextView tvAvg = findViewById(R.id.PointsScoredPerMatch);
            TextView tvAvgOpp = findViewById(R.id.PointsConcededPerMatch);
            tvTries.setText(String.valueOf(result[0].intValue()));
            tvPenalties.setText(String.valueOf(result[1].intValue()));
            tvDrops.setText(String.valueOf(result[2].intValue()));
            tvAvg.setText(String.valueOf(result[3].intValue()));
            tvAvgOpp.setText(String.valueOf(result[4].intValue()));
        }

        private int calculNbTry(ArrayList<Match> listMatches){
            int sumTry = 0;
            for (Match match : listMatches) {
                if(match.getTeam1() == 1)
                    sumTry += match.getTries1();
                else if(match.getTeam2() == 1)
                    sumTry += match.getTries2();
            }
            return sumTry;
        }

        private int calculNbPenalty(ArrayList<Match> listMatches){
            int sumPenalty = 0;
            for (Match match : listMatches) {
                if(match.getTeam1() == 1)
                    sumPenalty += match.getPenalty1();
                else if(match.getTeam2() == 1)
                    sumPenalty += match.getPenalty2();
            }
            return sumPenalty;
        }

        private int calculNbDrop(ArrayList<Match> listMatches){
            int sumDrop = 0;
            for (Match match : listMatches) {
                if(match.getTeam1() == 1)
                    sumDrop += match.getDrop1();
                else if(match.getTeam2() == 1)
                    sumDrop += match.getDrop2();
            }
            return sumDrop;
        }

        private int calculAvgOpponent(ArrayList<Match> listMatch) {
            int sumScore = 0;
            int sumMatch = 0;
            for (Match match : listMatch) {
                if(match.getTeam1() == 1) {
                    sumScore += match.getScore2();
                    sumMatch++;
                }
                else if(match.getTeam2() == 1) {
                    sumScore += match.getScore1();
                    sumMatch++;
                }
            }
            if(sumMatch == 0)
                return 0;
            return sumScore/sumMatch;
        }

        private int calculAvgScore(ArrayList<Match> listMatch) {
            int sumScore = 0;
            int sumMatch = 0;
            for (Match match : listMatch) {
                if(match.getTeam1() == 1) {
                    sumScore += match.getScore1();
                    sumMatch++;
                }
                else if(match.getTeam2() == 1) {
                    sumScore += match.getScore2();
                    sumMatch++;
                }
            }
            if(sumMatch == 0)
                return 0;
            return sumScore/sumMatch;
        }
    }
}
