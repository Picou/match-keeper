package fr.picou.matchkeeper.utils;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import fr.picou.matchkeeper.R;
import fr.picou.matchkeeper.database.SQLiteHelper;
import fr.picou.matchkeeper.database.model.Match;
import fr.picou.matchkeeper.match_details.MatchDetailsActivity;

public class MatchAdapter extends ArrayAdapter<Match> {

    private ArrayList<Match> matchs;
    private Context mContext;

    public MatchAdapter(ArrayList<Match> matchs, Context context) {
        super(context, R.layout.row_item, matchs);
        this.matchs = matchs;
        this.mContext= context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Match match = matchs.get(position);
        if(convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.row_item, null);
            Button btnDetails = convertView.findViewById(R.id.btnDetailsRow);
            btnDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getId() == R.id.btnDetailsRow) {
                        Button listItem = (Button) v;
                        Intent matchdetails = new Intent(mContext, MatchDetailsActivity.class);
                        matchdetails.putExtra("match", match.getId());
                        mContext.startActivity(matchdetails);
                    }
                }
            });
            SQLiteHelper sqLiteHelper = new SQLiteHelper(mContext);
            TextView score1 = convertView.findViewById(R.id.ScoreTeam1_row);
            score1.setText((String.valueOf(match.getScore1())));
            TextView score2 = convertView.findViewById(R.id.ScoreTeam2);
            score2.setText(String.valueOf(match.getScore2()));
            TextView team1 = convertView.findViewById(R.id.NomTeam1);
            team1.setText(sqLiteHelper.getTeam(match.getTeam1()).getName());
            TextView team2 = convertView.findViewById(R.id.NomTeam2);
            team2.setText(sqLiteHelper.getTeam(match.getTeam2()).getName());
            TextView date = convertView.findViewById(R.id.date_row);
            date.setText(match.getDate().toString());
            TextView duration = convertView.findViewById(R.id.duree_row);
            duration.setText(String.valueOf(match.getDuration()));
            sqLiteHelper.close();
        }
        return convertView;
    }
}
