package fr.picou.matchkeeper.database;

import android.os.AsyncTask;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import fr.picou.matchkeeper.database.model.Match;

public class ExternalMySQLAsyncTask extends AsyncTask<String, Void, ResultSet> {
    @Override
    protected ResultSet doInBackground(String[] strings) {
        return requestToExternalDB(strings[0]);
    }

    private ResultSet requestToExternalDB(String req) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager.getConnection("jdbc:mysql://192.168.1.15/android?autoReconnect=true", "root", "");
            if(connection != null) {
                Statement stmt = connection.createStatement();
                ResultSet rs = stmt.executeQuery(req);
                if(rs != null) {
                    ArrayList<Match> matchs = new ArrayList<>();
                    while (rs.next()) {
                        matchs.add(new Match(rs.getLong("id"), rs.getLong("team1"),
                                rs.getLong("team2"), rs.getInt("score1"),
                                rs.getInt("score2"), rs.getInt("try1"),
                                rs.getInt("try2"), rs.getInt("conversion1"),
                                rs.getInt("conversion2"), rs.getInt("drop1"),
                                rs.getInt("drop2"), rs.getInt("penalty1"),
                                rs.getInt("penalty2"), rs.getDouble("latitude"),
                                rs.getDouble("longitude"), rs.getInt("duration"),
                                new Date(rs.getDate("date").getTime())));
                        Log.println(Log.INFO, "", "test mysql");
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }
}
