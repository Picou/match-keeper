package fr.picou.matchkeeper.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Date;

import fr.picou.matchkeeper.database.model.Match;
import fr.picou.matchkeeper.database.model.Team;

public class SQLiteHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "match.db";
    private static final String DATABASE_DROP_TABLE = "DROP TABLE IF EXISTS ";
    private static final String DATABASE_COUNT = "SELECT COUNT(*) FROM ";
    private static final String SELECT_FROM = "SELECT * FROM ";
    private static final String ORDER_BY = " ORDER BY ";
    private static final String LATEST = " DESC LIMIT 1";

    public SQLiteHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Match.CREATE_TABLE);
        db.execSQL(Team.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DATABASE_DROP_TABLE + Match.TABLE_NAME);
        db.execSQL(DATABASE_DROP_TABLE + Team.TABLE_NAME);

        onCreate(db);
    }

    public long insertMatch(Match match) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Match.COLUMN_CONVERSION_TEAM1, match.getConversion1());
        values.put(Match.COLUMN_CONVERSION_TEAM2, match.getConversion2());
        values.put(Match.COLUMN_DATE, match.getDate().toString());
        values.put(Match.COLUMN_DURATION, match.getDate().toString());
        values.put(Match.COLUMN_DROP_TEAM1, match.getDrop1());
        values.put(Match.COLUMN_DROP_TEAM2, match.getDrop2());
        values.put(Match.COLUMN_LATITUDE, match.getLatitude());
        values.put(Match.COLUMN_LONGITUDE, match.getLongitude());
        values.put(Match.COLUMN_TEAM1, match.getTeam1());
        values.put(Match.COLUMN_TEAM2, match.getTeam2());
        values.put(Match.COLUMN_PENALTY_TEAM1, match.getPenalty1());
        values.put(Match.COLUMN_PENALTY_TEAM2, match.getPenalty2());
        values.put(Match.COLUMN_SCORE_TEAM1, match.getScore1());
        values.put(Match.COLUMN_SCORE_TEAM2, match.getScore2());
        values.put(Match.COLUMN_TRY_TEAM1, match.getTries1());
        values.put(Match.COLUMN_TRY_TEAM2, match.getTries2());

        // TODO also insert in external db

        long id = db.insert(Match.TABLE_NAME, null, values);

        //check if more than five
        if(countMatchRows() > 5) {
            deleteEarliestMatch();
        }

        db.close();

        return id;
    }

    private long insertTeam(Team team) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Team.COLUMN_TEAM_NAME, team.getName());

        long id = db.insert(Team.TABLE_NAME, null, values);

        db.close();
        return id;
    }

    private long countMatchRows() {
        String sql = DATABASE_COUNT + Match.TABLE_NAME;
        SQLiteStatement statement = getReadableDatabase().compileStatement(sql);
        return statement.simpleQueryForLong();
    }

    private void deleteEarliestMatch() {
        SQLiteDatabase dbRead = this.getReadableDatabase();

        Cursor cursor = dbRead.query(Match.TABLE_NAME, null,
                "min(" + Match.COLUMN_ID + ")" , null, null, null, null);
        dbRead.close();



        if (cursor != null) {
            cursor.moveToFirst();
            int idToDelete = cursor.getInt(0);
            cursor.close();

            SQLiteDatabase dbWrite = this.getWritableDatabase();
            dbWrite.delete(Match.TABLE_NAME, Match.COLUMN_ID + " = ?",
                    new String[]{String.valueOf(idToDelete)});
            dbWrite.close();
        }

    }

    public Team createOrRetrieveTeam(String name) {
        SQLiteDatabase dbReadCount = getReadableDatabase();
        String sql = DATABASE_COUNT + Team.TABLE_NAME + " WHERE name = '" + name + "'";
        SQLiteStatement statement = dbReadCount.compileStatement(sql);
        if(statement.simpleQueryForLong() > 0) {
            dbReadCount.close();

            //get existing team
            SQLiteDatabase dbRead = getReadableDatabase();
            Cursor cursor = dbRead.query(Team.TABLE_NAME, null, Team.COLUMN_TEAM_NAME + " = ?", new String[]{ name }, null, null, null);
            cursor.moveToFirst();
            int idIndex = cursor.getColumnIndex(Team.COLUMN_ID);
            int nameIndex = cursor.getColumnIndex(Team.COLUMN_TEAM_NAME);
            Team existingTeam = new Team(cursor.getLong(idIndex), cursor.getString(nameIndex));
            cursor.close();
            dbRead.close();
            return existingTeam;
        }
        else{
            dbReadCount.close();

            long id = insertTeam(new Team(name));
            SQLiteDatabase dbRead = getReadableDatabase();
            Cursor cursor = dbRead.query(Team.TABLE_NAME, null, Team.COLUMN_ID + " = ?", new String[]{ ( String.valueOf(id)) }, null, null, null);
            cursor.moveToFirst();
            int idIndex = cursor.getColumnIndex(Team.COLUMN_ID);
            int nameIndex = cursor.getColumnIndex(Team.COLUMN_TEAM_NAME);
            Team newTeam = new Team(cursor.getLong(idIndex), cursor.getString(nameIndex));
            cursor.close();
            dbRead.close();
            return newTeam;
        }
    }

    public Match getMatch(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(Match.TABLE_NAME, null, Match.COLUMN_ID + " = ?", new String[]{ String.valueOf(id) }, null, null, null);
        cursor.moveToFirst();

        Match match = null; // case something went wrong
        if(countMatchRows() > 0)
            match = new Match(cursor.getLong(cursor.getColumnIndex(Match.COLUMN_ID)),
                    cursor.getLong(cursor.getColumnIndex(Match.COLUMN_TEAM1)),
                    cursor.getLong(cursor.getColumnIndex(Match.COLUMN_TEAM2)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_SCORE_TEAM1)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_SCORE_TEAM2)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_TRY_TEAM1)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_TRY_TEAM2)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_CONVERSION_TEAM1)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_CONVERSION_TEAM2)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_DROP_TEAM1)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_DROP_TEAM2)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_PENALTY_TEAM1)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_PENALTY_TEAM2)),
                    cursor.getDouble(cursor.getColumnIndex(Match.COLUMN_LATITUDE)),
                    cursor.getDouble(cursor.getColumnIndex(Match.COLUMN_LONGITUDE)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_DURATION)),
                    new Date(cursor.getLong(cursor.getColumnIndex(Match.COLUMN_DATE))*1000)); // really annoying date parsing

        cursor.close();
        db.close();
        return match;
    }

    public Match getLatestMatch() {
        String selectQuery= SELECT_FROM + Match.TABLE_NAME + ORDER_BY + Match.COLUMN_ID + LATEST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();

        Match latest = null; // case something went wrong
        if(countMatchRows() > 0)
            latest = new Match(cursor.getLong(cursor.getColumnIndex(Match.COLUMN_ID)),
                    cursor.getLong(cursor.getColumnIndex(Match.COLUMN_TEAM1)),
                    cursor.getLong(cursor.getColumnIndex(Match.COLUMN_TEAM2)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_SCORE_TEAM1)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_SCORE_TEAM2)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_TRY_TEAM1)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_TRY_TEAM2)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_CONVERSION_TEAM1)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_CONVERSION_TEAM2)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_DROP_TEAM1)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_DROP_TEAM2)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_PENALTY_TEAM1)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_PENALTY_TEAM2)),
                    cursor.getDouble(cursor.getColumnIndex(Match.COLUMN_LATITUDE)),
                    cursor.getDouble(cursor.getColumnIndex(Match.COLUMN_LONGITUDE)),
                    cursor.getInt(cursor.getColumnIndex(Match.COLUMN_DURATION)),
                    new Date(cursor.getLong(cursor.getColumnIndex(Match.COLUMN_DATE))*1000)); // really annoying date parsing

        cursor.close();
        db.close();
        return latest;
    }

    public Team getTeam(long id) {
        SQLiteDatabase dbRead = getReadableDatabase();
        Cursor cursor = dbRead.query(Team.TABLE_NAME, null, Team.COLUMN_ID + " = ?", new String[]{ String.valueOf(id) }, null, null, null);
        cursor.moveToFirst();
        int idIndex = cursor.getColumnIndex(Team.COLUMN_ID);
        int nameIndex = cursor.getColumnIndex(Team.COLUMN_TEAM_NAME);
        Team team = new Team(cursor.getLong(idIndex), cursor.getString(nameIndex));
        cursor.close();
        dbRead.close();
        return team;
    }


    public ArrayList<Match> getMatches(){
        ArrayList<Match> listMatches = new ArrayList<Match>();
        String selectALLQuery = SELECT_FROM + Match.TABLE_NAME;

        SQLiteDatabase dbRead = getReadableDatabase();
        Cursor result = dbRead.rawQuery(selectALLQuery, null);

        result.moveToFirst();
        while(!result.isAfterLast()){
            Match match = new Match(result.getLong(result.getColumnIndex(Match.COLUMN_ID)),
                    result.getLong(result.getColumnIndex(Match.COLUMN_TEAM1)),
                    result.getLong(result.getColumnIndex(Match.COLUMN_TEAM2)),
                    result.getInt(result.getColumnIndex(Match.COLUMN_SCORE_TEAM1)),
                    result.getInt(result.getColumnIndex(Match.COLUMN_SCORE_TEAM2)),
                    result.getInt(result.getColumnIndex(Match.COLUMN_TRY_TEAM1)),
                    result.getInt(result.getColumnIndex(Match.COLUMN_TRY_TEAM2)),
                    result.getInt(result.getColumnIndex(Match.COLUMN_CONVERSION_TEAM1)),
                    result.getInt(result.getColumnIndex(Match.COLUMN_CONVERSION_TEAM2)),
                    result.getInt(result.getColumnIndex(Match.COLUMN_DROP_TEAM1)),
                    result.getInt(result.getColumnIndex(Match.COLUMN_DROP_TEAM2)),
                    result.getInt(result.getColumnIndex(Match.COLUMN_PENALTY_TEAM1)),
                    result.getInt(result.getColumnIndex(Match.COLUMN_PENALTY_TEAM2)),
                    result.getDouble(result.getColumnIndex(Match.COLUMN_LATITUDE)),
                    result.getDouble(result.getColumnIndex(Match.COLUMN_LONGITUDE)),
                    result.getInt(result.getColumnIndex(Match.COLUMN_DURATION)),
                    new Date(result.getLong(result.getColumnIndex(Match.COLUMN_DATE))*1000));
            listMatches.add(match);
            result.moveToNext();
        }
        result.close();
        dbRead.close();
        return listMatches;
    }


}
