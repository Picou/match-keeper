package fr.picou.matchkeeper.database.model;

import java.util.Date;

public class Match {
    public static final String TABLE_NAME = "match";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TEAM1 = "team1";
    public static final String COLUMN_TEAM2 = "team2";
    public static final String COLUMN_SCORE_TEAM1 = "score1";
    public static final String COLUMN_SCORE_TEAM2 = "score2";
    public static final String COLUMN_TRY_TEAM1 = "try1";
    public static final String COLUMN_TRY_TEAM2 = "try2";
    public static final String COLUMN_CONVERSION_TEAM1 = "conversion1";
    public static final String COLUMN_CONVERSION_TEAM2 = "conversion2";
    public static final String COLUMN_DROP_TEAM1 = "drop1";
    public static final String COLUMN_DROP_TEAM2 = "drop2";
    public static final String COLUMN_PENALTY_TEAM1 = "penalty1";
    public static final String COLUMN_PENALTY_TEAM2 = "penalty2";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longitude";
    public static final String COLUMN_DURATION = "duration";
    public static final String COLUMN_DATE = "date";

    private long id;
    private long team1;
    private long team2;
    private int score1;
    private int score2;
    private int tries1;
    private int tries2;
    private int conversion1;
    private int conversion2;
    private int drop1;
    private int drop2;
    private int penalty1;
    private int penalty2;
    private double latitude;
    private double longitude;
    private int duration;
    private Date date;

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_TEAM1 + " INTEGER,"
                    + COLUMN_TEAM2 + " INTEGER,"
                    + COLUMN_SCORE_TEAM1 + " INTEGER,"
                    + COLUMN_SCORE_TEAM2 + " INTEGER,"
                    + COLUMN_TRY_TEAM1 + " INTEGER,"
                    + COLUMN_TRY_TEAM2 + " INTEGER,"
                    + COLUMN_DROP_TEAM1 + " INTEGER,"
                    + COLUMN_DROP_TEAM2 + " INTEGER,"
                    + COLUMN_CONVERSION_TEAM1 + " INTEGER,"
                    + COLUMN_CONVERSION_TEAM2 + " INTEGER,"
                    + COLUMN_PENALTY_TEAM1 + " INTEGER,"
                    + COLUMN_PENALTY_TEAM2 + " INTEGER,"
                    + COLUMN_LATITUDE + " DOUBLE,"
                    + COLUMN_LONGITUDE + " DOUBLE,"
                    + COLUMN_DURATION + " VARCHAR(50),"
                    + COLUMN_DATE + " DATE DEFAULT CURRENT_TIMESTAMP"
                    + ")";

    public Match(long id, long team1, long team2, int score1, int score2, int tries1, int tries2,
                 int conversion1, int conversion2, int drop1, int drop2, int penalty1, int penalty2,
                 double latitude, double longitude, int duration, Date date) {
        this.id = id;
        this.team1 = team1;
        this.team2 = team2;
        this.score1 = score1;
        this.score2 = score2;
        this.tries1 = tries1;
        this.tries2 = tries2;
        this.conversion1 = conversion1;
        this.conversion2 = conversion2;
        this.drop1 = drop1;
        this.drop2 = drop2;
        this.penalty1 = penalty1;
        this.penalty2 = penalty2;
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
        this.duration = duration;
    }

    public Match(long team1, long team2, int score1, int score2, int tries1, int tries2,
                 int conversion1, int conversion2, int drop1, int drop2, int penalty1, int penalty2,
                 double latitude, double longitude, int duration, Date date) {
        this.team1 = team1;
        this.team2 = team2;
        this.score1 = score1;
        this.score2 = score2;
        this.tries1 = tries1;
        this.tries2 = tries2;
        this.conversion1 = conversion1;
        this.conversion2 = conversion2;
        this.drop1 = drop1;
        this.drop2 = drop2;
        this.penalty1 = penalty1;
        this.penalty2 = penalty2;
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
        this.duration = duration;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public long getTeam1() {
        return team1;
    }

    public void setTeam1(int team1) {
        this.team1 = team1;
    }

    public long getTeam2() {
        return team2;
    }

    public void setTeam2(int team2) {
        this.team2 = team2;
    }

    public int getTries1() {
        return tries1;
    }

    public void setTries1(int tries1) {
        this.tries1 = tries1;
    }

    public int getTries2() {
        return tries2;
    }

    public void setTries2(int tries2) {
        this.tries2 = tries2;
    }

    public int getConversion1() {
        return conversion1;
    }

    public void setConversion1(int conversion1) {
        this.conversion1 = conversion1;
    }

    public int getConversion2() {
        return conversion2;
    }

    public void setConversion2(int conversion2) {
        this.conversion2 = conversion2;
    }

    public int getDrop1() {
        return drop1;
    }

    public void setDrop1(int drop1) {
        this.drop1 = drop1;
    }

    public int getDrop2() {
        return drop2;
    }

    public void setDrop2(int drop2) {
        this.drop2 = drop2;
    }

    public int getPenalty1() {
        return penalty1;
    }

    public void setPenalty1(int penalty1) {
        this.penalty1 = penalty1;
    }

    public int getPenalty2() {
        return penalty2;
    }

    public void setPenalty2(int penalty2) {
        this.penalty2 = penalty2;
    }

    public int getScore1() {
        return score1;
    }

    public void setScore1(int score1) {
        this.score1 = score1;
    }

    public int getScore2() {
        return score2;
    }

    public void setScore2(int score2) {
        this.score2 = score2;
    }
}
