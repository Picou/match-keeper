package fr.picou.matchkeeper.database.model;

public class Team {
    public static final String TABLE_NAME = "team";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TEAM_NAME = "name";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_TEAM_NAME + " VARCHAR(50)"
            + ")";

    private long id;
    private String name;

    public Team(String name) {
        this.name = name;
    }

    public Team(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
