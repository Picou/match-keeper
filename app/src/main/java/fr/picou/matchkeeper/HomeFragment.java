package fr.picou.matchkeeper;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import fr.picou.matchkeeper.home.BtnNewMatchFragment;
import fr.picou.matchkeeper.home.LastMatchFragment;
import fr.picou.matchkeeper.home.OnBtnNewGameFragmentInteractionListener;
import fr.picou.matchkeeper.home.OnLastMatchFragmentInteractionListener;


public class HomeFragment extends Fragment implements OnLastMatchFragmentInteractionListener, OnBtnNewGameFragmentInteractionListener {

    private OnHomeFragmentInteractionListener mListener;
    private View view;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_home, container, false);

        LastMatchFragment frag1 = new LastMatchFragment();
        getChildFragmentManager().beginTransaction().add(R.id.LastGameFrame, frag1).commit();
        BtnNewMatchFragment frag2 = new BtnNewMatchFragment();
        getChildFragmentManager().beginTransaction().add(R.id.AddANewGameFrame, frag2).commit();

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnHomeFragmentInteractionListener) {
            mListener = (OnHomeFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void displayStatFragment() {
        mListener.displayStatFragment();
    }

    @Override
    public void displayLastMatch() {
        mListener.displayLastMatch();
    }

    @Override
    public void displayAddNewMatchActivity() {
        mListener.displayAddNewMatchActivity();
    }


}
