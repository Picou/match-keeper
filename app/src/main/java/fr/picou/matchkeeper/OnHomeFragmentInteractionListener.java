package fr.picou.matchkeeper;

interface OnHomeFragmentInteractionListener {
    void displayStatFragment();

    void displayAddNewMatchActivity();

    void displayLastMatch();
}
