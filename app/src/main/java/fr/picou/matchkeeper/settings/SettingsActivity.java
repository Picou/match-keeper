package fr.picou.matchkeeper.settings;


import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import fr.picou.matchkeeper.R;


public class SettingsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings_layout, new SettingsFragment())
                .commit();
    }
}
