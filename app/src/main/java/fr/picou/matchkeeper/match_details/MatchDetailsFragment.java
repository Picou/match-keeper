package fr.picou.matchkeeper.match_details;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.text.SimpleDateFormat;
import java.util.Locale;

import fr.picou.matchkeeper.R;
import fr.picou.matchkeeper.database.SQLiteHelper;
import fr.picou.matchkeeper.database.model.Match;

/**
 * A simple {@link Fragment} subclass.
 */
public class MatchDetailsFragment extends Fragment{

    private OnDetailsMatchFragmentInteractionListener mListener;
    private View view;
    private long matchId;
    private SQLiteHelper sqlLiteHelper;

    public MatchDetailsFragment(){
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        view  = inflater.inflate(R.layout.fragment_match_details, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            matchId = bundle.getLong("match_id", -1);
        }
        Button btnBack = view.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.goBack();
            }
        });

        if(matchId != -1) {
            setTextViews(view);
        }

        return view;


    }

    private void setTextViews(View view) {
            Match match = sqlLiteHelper.getMatch(matchId);
            TextView scoreTeam1 = view.findViewById(R.id.scoreTeam1det);
            scoreTeam1.setText(String.valueOf(match.getScore1()));
            TextView scoreTeam2 = view.findViewById(R.id.scoreTeam2det);
            scoreTeam2.setText(String.valueOf(match.getScore2()));
            TextView team1 = view.findViewById(R.id.nomTeam1det);
            team1.setText(sqlLiteHelper.getTeam(match.getTeam1()).getName());
            TextView team2 = view.findViewById(R.id.nomTeam2det);
            team2.setText(sqlLiteHelper.getTeam(match.getTeam2()).getName());
            TextView essaiTeam1 = view.findViewById(R.id.essaiTeam1det);
            essaiTeam1.setText(String.valueOf(match.getTries1()));
            TextView essaiTeam2 = view.findViewById(R.id.essaiTeam2det);
            essaiTeam2.setText(String.valueOf(match.getTries2()));
            TextView transTeam1 = view.findViewById(R.id.transTeam1det);
            transTeam1.setText(String.valueOf(match.getConversion1()));
            TextView transTeam2 = view.findViewById(R.id.transTeam2det);
            transTeam2.setText(String.valueOf(match.getConversion2()));
            TextView drop1 = view.findViewById(R.id.dropTeam1det);
            drop1.setText(String.valueOf(match.getDrop1()));
            TextView drop2 = view.findViewById(R.id.dropTeam2det);
            drop2.setText(String.valueOf(match.getDrop2()));
            TextView pen1 = view.findViewById(R.id.penaliteTeam1det);
            pen1.setText(String.valueOf(match.getPenalty1()));
            TextView pen2 = view.findViewById(R.id.penaliteTeam2det);
            pen2.setText(String.valueOf(match.getPenalty2()));
            TextView date = view.findViewById(R.id.date_rowdet);
            date.setText(new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(match.getDate()));
            TextView dur = view.findViewById(R.id.duree_rowdet);
            dur.setText(String.valueOf(match.getDuration()));
            sqlLiteHelper.close();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDetailsMatchFragmentInteractionListener) {
            this.mListener = (OnDetailsMatchFragmentInteractionListener) context;
            this.sqlLiteHelper = new SQLiteHelper(context);
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddNewGameFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
