package fr.picou.matchkeeper.match_details;

interface OnDetailsMatchFragmentInteractionListener {
    void goBack();
}
