package fr.picou.matchkeeper.match_details;


import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import fr.picou.matchkeeper.R;


public class MatchDetailsActivity extends AppCompatActivity implements OnDetailsMatchFragmentInteractionListener {
    Intent intent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_details);

        this.intent = getIntent();
        Bundle bundle = new Bundle();
        bundle.putLong("match_id", intent.getLongExtra("match", -1));
        MatchDetailsFragment frag = new MatchDetailsFragment();
        frag.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(R.id.host_match_details, frag).commit();


    }



    @Override
    public void goBack() {
        super.onBackPressed();
    }
}
