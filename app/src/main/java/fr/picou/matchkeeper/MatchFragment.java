package fr.picou.matchkeeper;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import fr.picou.matchkeeper.database.SQLiteHelper;
import fr.picou.matchkeeper.utils.MatchAdapter;


public class MatchFragment extends Fragment {

    private OnMatchFragmentInteractionListener mListener;
    private SQLiteHelper sqlLiteHelper;
    private static MatchAdapter adapter;
    private Context context;

    public MatchFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_match, container, false);
        //mListener.setAllMatchsSummary();

        ListView listView = (ListView) view.findViewById(R.id.list);
        adapter = new MatchAdapter(sqlLiteHelper.getMatches(), context);
        listView.setAdapter(adapter);


        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMatchFragmentInteractionListener) {
            mListener = (OnMatchFragmentInteractionListener) context;
            this.context = context;
            this.sqlLiteHelper = new SQLiteHelper(context);
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        context = null;
    }

}
