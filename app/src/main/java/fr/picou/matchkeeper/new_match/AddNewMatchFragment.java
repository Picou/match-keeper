package fr.picou.matchkeeper.new_match;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import fr.picou.matchkeeper.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddNewMatchFragment extends Fragment {

    private OnAddNewGameFragmentInteractionListener context;
    private View view;

    public AddNewMatchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_add_new_match, container, false);

        Button btnNewMatch = view.findViewById(R.id.btnSaveGame);
        btnNewMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveGame();
            }
        });

        Button btnTakePic = view.findViewById(R.id.btnTakePic);
        btnTakePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });

        return view;
    }

    private void takePicture() {
        context.takePicture();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddNewGameFragmentInteractionListener) {
            this.context = (OnAddNewGameFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddNewGameFragmentInteractionListener");
        }
    }

    private void saveGame() {
        EditText scoreTeam1 = this.view.findViewById(R.id.editTextScoreTeam1);
        EditText scoreTeam2 = this.view.findViewById(R.id.editTextScoreTeam2);
        EditText nameTeam1 = this.view.findViewById(R.id.editTextNomTeam1);
        EditText nameTeam2 = this.view.findViewById(R.id.editTextNomTeam2);
        EditText tryTeam1 = this.view.findViewById(R.id.editTextEssaiTeam1);
        EditText tryTeam2 = this.view.findViewById(R.id.editTextEssaiTeam2);
        EditText convTeam1 = this.view.findViewById(R.id.editTextTransTeam1);
        EditText convTeam2 = this.view.findViewById(R.id.editTextTransTeam2);
        EditText dropTeam1 = this.view.findViewById(R.id.editTextDropTeam1);
        EditText dropTeam2 = this.view.findViewById(R.id.editTextDropTeam2);
        EditText penaltyTeam1 = this.view.findViewById(R.id.editTextPenaliteTeam1);
        EditText penaltyTeam2 = this.view.findViewById(R.id.editTextPenaliteTeam2);
        EditText date = this.view.findViewById(R.id.editTextDate);
        EditText duration = this.view.findViewById(R.id.editTextDuree);

        if(!scoreTeam1.getText().toString().equals("") && !scoreTeam2.getText().toString().equals("")
        && !nameTeam1.getText().toString().equals("") && !nameTeam2.getText().toString().equals("")
        && !tryTeam1.getText().toString().equals("") && !tryTeam2.getText().toString().equals("")
        && !convTeam1.getText().toString().equals("") && !convTeam2.getText().toString().equals("")
        && !dropTeam1.getText().toString().equals("") && !dropTeam2.getText().toString().equals("")
        && !penaltyTeam1.getText().toString().equals("") && !penaltyTeam2.getText().toString().equals("")
        && !date.getText().toString().equals("") && !duration.getText().toString().equals("")) {

            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

            try {
                context.saveGame(Integer.parseInt(scoreTeam1.getText().toString()),
                        Integer.parseInt(scoreTeam2.getText().toString()),
                        nameTeam1.getText().toString(), nameTeam2.getText().toString(),
                        Integer.parseInt(tryTeam1.getText().toString()),
                        Integer.parseInt(tryTeam2.getText().toString()),
                        Integer.parseInt(convTeam1.getText().toString()),
                        Integer.parseInt(convTeam2.getText().toString()),
                        Integer.parseInt(dropTeam1.getText().toString()),
                        Integer.parseInt(dropTeam2.getText().toString()),
                        Integer.parseInt(penaltyTeam1.getText().toString()),
                        Integer.parseInt(penaltyTeam2.getText().toString()),
                        format.parse(date.getText().toString()), Integer.parseInt(duration.getText().toString()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        context = null;
    }
}
