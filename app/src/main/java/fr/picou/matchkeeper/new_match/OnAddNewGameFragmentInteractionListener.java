package fr.picou.matchkeeper.new_match;

import java.util.Date;

interface OnAddNewGameFragmentInteractionListener {
    void saveGame(int score1, int score2, String team1, String team2, int try1, int try2, int conv1, int conv2, int drop1, int drop2, int penalty1, int penalty2, Date date, int duration);

    void takePicture();
}
