package fr.picou.matchkeeper.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import fr.picou.matchkeeper.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BtnNewMatchFragment extends Fragment {

    private OnBtnNewGameFragmentInteractionListener context;

    public BtnNewMatchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // regular onAttach doesn't work in nested fragments
        onAttachToParentFragment(getParentFragment());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_btn_new_match, container, false);

        Button btnNewMatch = view.findViewById(R.id.newMatchButton);
        btnNewMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayAddNewMatchActivity();
            }
        });

        return view;
    }

    private void displayAddNewMatchActivity() {
        context.displayAddNewMatchActivity();
    }

    private void onAttachToParentFragment(Fragment fragment)
    {
        try
        {
            context = (OnBtnNewGameFragmentInteractionListener)fragment;

        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(
                    fragment.toString() + " must implement OnBtnNewGameFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        context = null;
    }
}
