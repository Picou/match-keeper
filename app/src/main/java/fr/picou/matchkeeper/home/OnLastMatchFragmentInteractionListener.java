package fr.picou.matchkeeper.home;

public interface OnLastMatchFragmentInteractionListener {
    void displayStatFragment();

    void displayLastMatch();
}
