package fr.picou.matchkeeper.home;

public interface OnBtnNewGameFragmentInteractionListener {

    void displayAddNewMatchActivity();

}
