package fr.picou.matchkeeper.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import fr.picou.matchkeeper.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LastMatchFragment extends Fragment {

    private OnLastMatchFragmentInteractionListener context;

    public LastMatchFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // regular onAttach doesn't work nested fragments
        onAttachToParentFragment(getParentFragment());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_last_match, container, false);

        Button btnSeeMore = view.findViewById(R.id.btnSeeMore);
        btnSeeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayStatFragment();
            }
        });


        return view;
    }

    private void displayStatFragment() {
        context.displayStatFragment();
    }

    private void onAttachToParentFragment(Fragment fragment)
    {
        try
        {
            context = (OnLastMatchFragmentInteractionListener)fragment;

        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(
                    fragment.toString() + " must implement OnLastMatchFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        context = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        context.displayLastMatch();
    }
}
